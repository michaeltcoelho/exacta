.PHONY: clean
clean:
	@find . -regex "\(.*__pycache__.*\|*.py[co]\)" -delete

.PHONY: install
install: clean
	@pip install -e ".[testing]"

.PHONY: initdb
initdb:
	@init_db development.ini

.PHONY: run
run:
	@pserve development.ini --reload

.PHONY: run_tests
run_tests:
	@pytest -s
