import random
import re

import requests
from requests.exceptions import HTTPError

from src.models.quotes import Quote, QuoteNotFound


quotes_api_exceptions = {
    404: QuoteNotFound,
}


class QuotesClient:
    BASE_URI = 'https://1c22eh3aj8.execute-api.us-east-1.amazonaws.com/challenge'

    def __init__(self):
        self.session = requests.Session()
        self.session.headers.update({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        })

    def make_uri(self, *paths):
        uri = self.BASE_URI
        for path in paths:
            uri = re.sub(r'/?$', re.sub(r'^/?', '/', str(path)), uri)
        return uri

    def _handle_response(self, response):
        response.raise_for_status()
        return response.json()

    def get(self, *path):
        response = self.session.get(self.make_uri(*path))
        return self._handle_response(response)


class QuotesAPI:

    def __init__(self, client=None):
        self.client = client or QuotesClient()

    def get_quotes(self):
        quotes = self.client.get('/quotes')
        return [Quote(quote=quote) for quote in quotes['quotes']]

    def get_quote(self, quote_number):
        try:
            quote = self.client.get('/quotes', quote_number)
        except HTTPError as ex:
            raise quotes_api_exceptions[ex.response.status_code] from ex
        else:
            return Quote(quote=quote['quote'])


class APIQuotesService:

    def get_quotes(self):
        quotes_api = QuotesAPI()
        return quotes_api.get_quotes()

    def get_quote(self, quote_number):
        quotes_api = QuotesAPI()
        return quotes_api.get_quote(quote_number)

    def get_random_quote(self):
        quotes_api = QuotesAPI()
        quotes = quotes_api.get_quotes()
        rand_index = random.randint(0, len(quotes) - 1)
        return quotes[rand_index]
