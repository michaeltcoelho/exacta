from src.services import quotes
from src.services import session


def register_service_and_inject_uow(config, service, name):
    config.register_service_factory(
        lambda ctx, req: service(req.unit_of_work),
        name=name)


def includeme(config):
    config.register_service(quotes.APIQuotesService(), name='quotes')
    register_service_and_inject_uow(
        config, session.SessionService, 'session')
    register_service_and_inject_uow(
        config, session.SessionRequestService, 'sessionrequest')
