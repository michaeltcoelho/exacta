import os
import binascii
from datetime import datetime

from src.models.session import Session, SessionRequest


def random_token(length=32):
    return binascii.hexlify(os.urandom(length)).decode('utf-8')


class SessionService:

    def __init__(self, unit_of_work):
        self.unit_of_work = unit_of_work

    def create_new_session(self, browser):
        with self.unit_of_work as uow:
            session = Session(random_token(), browser)
            session = uow.session_repository.add(session)
            uow.commit()

    def get_session_by_browser(self, browser):
        with self.unit_of_work as uow:
            session = uow.session_repository.get_by_browser(browser)
        return session

    def get_sessions(self):
        with self.unit_of_work as uow:
            sessions = uow.session_repository.all()
        return sessions


class SessionRequestService:

    def __init__(self, unit_of_work):
        self.unit_of_work = unit_of_work

    def add_new_sessionrequest(self, session, requested_url):
        with self.unit_of_work as uow:
            sessionrequest = SessionRequest(
                requested_at=datetime.utcnow(),
                requested_url=requested_url)
            session.requests.append(sessionrequest)
            uow.session_repository.add(session)
            uow.commit()

    def get_sessionrequests_by_session_id(self, session_id):
        with self.unit_of_work as uow:
            sessionrequests = uow.sessionrequest_repository\
                .get_by_session_id(session_id)
        return sessionrequests
