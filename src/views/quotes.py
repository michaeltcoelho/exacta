from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config, notfound_view_config

from src.models.quotes import QuoteNotFound


@notfound_view_config(renderer='../templates/404.html')
def handle_not_found(request):
    return {}


@view_config(route_name='home', renderer='../templates/home.html')
def home(request):
    return {'version': '1.0'}


@view_config(route_name='quotes', renderer='../templates/quotes.html')
def quotes(request):
    quotes_svc = request.find_service(name='quotes')
    quotes = quotes_svc.get_quotes()
    return {'quotes': quotes}


@view_config(route_name='quote', renderer='../templates/quote.html')
def quote(request):
    quote_number = request.matchdict['quote_number']
    try:
        quotes_svc = request.find_service(name='quotes')
        quote = quotes_svc.get_quote(quote_number)
    except QuoteNotFound as ex:
        raise HTTPNotFound()
    else:
        return {'quote': quote}


@view_config(route_name='random_quote', renderer='../templates/quote.html')
def random_quote(request):
    quotes_svc = request.find_service(name='quotes')
    quote = quotes_svc.get_random_quote()
    return {'quote': quote}
