from . import quotes
from . import session


def includeme(config):
    config.add_route('home', '/')
    config.add_route('quotes', '/quotes')
    config.add_route('random_quote', '/quotes/random')
    config.add_route('quote', '/quotes/{quote_number}')
    config.add_route('get_sessions', '/sessions')
    config.add_route('get_sessionrequests_by_session_id',
                     '/sessions/{session_id}/requests')

    config.add_subscriber(
        'src.views.session.new_request_handler',
        'pyramid.events.NewRequest')
