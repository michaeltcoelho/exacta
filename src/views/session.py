from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config

from src.models.session import SessionDoesNotExist


def get_browser_family_from_request(request):
    user_agent = request.user_agent_classified
    browser = user_agent.browser
    return browser.family


def new_request_handler(event):
    session_svc = event.request.find_service(name='session')
    session_request_svc = event.request\
        .find_service(name='sessionrequest')

    browser_family = get_browser_family_from_request(event.request)

    try:
        session = session_svc.get_session_by_browser(browser_family)
    except SessionDoesNotExist as ex:
        session_svc.create_new_session(browser_family)
        session = session_svc.get_session_by_browser(browser_family)

    session_request_svc.add_new_sessionrequest(session, event.request.url)


@view_config(route_name='get_sessions', renderer='json')
def get_sessions(request):
    session_svc = request.find_service(name='session')
    sessions = session_svc.get_sessions()
    response = []
    for session in sessions:
        response.append({
            'id': session.id,
            'token': session.token,
            'browser': session.browser,
        })
    return {'sessions': response}


@view_config(route_name='get_sessionrequests_by_session_id', renderer='json')
def get_sessionrequests_by_session_id(request):
    session_id = request.matchdict['session_id']
    try:
        sessionrequest_svc = request.find_service(name='sessionrequest')
        sessionrequests = sessionrequest_svc\
            .get_sessionrequests_by_session_id(session_id)
        response = []
        for sessionrequest in sessionrequests:
            response.append({
                'id': sessionrequest.id,
                'requested_at': sessionrequest.requested_at.strftime('%Y-%m-%d'),
                'requested_url': sessionrequest.requested_url,
            })
        return {'requests': response}
    except SessionDoesNotExist as ex:
        raise HTTPNotFound() from ex
