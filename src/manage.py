import sys

from pyramid.paster import get_appsettings

from src.adapters.orm import SQLAlchemy


def init_db(argv=None):
    if argv is None:
        argv = sys.argv

    if len(argv) < 2:
        raise ValueError('Usage: init_db <config_uri>')

    config_uri = argv[1]
    settings = get_appsettings(config_uri)

    db = SQLAlchemy.from_config(settings, prefix='sqlalchemy.')
    db.configure()
    db.create_schema()

    sys.stdout.write('Database initialized!!')
