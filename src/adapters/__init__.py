from .orm import SQLAlchemy


def includeme(config):
    db = SQLAlchemy.from_config(config.registry.settings, 'sqlalchemy.')
    db.configure()

    def request_method(request):
        def shutdown(request):
            request.unit_of_work.remove()
        request.add_finished_callback(shutdown)
        return db.unit_of_work

    config.add_request_method(
        request_method,
        'unit_of_work',
        reify=True,
    )
