import threading

from sqlalchemy import (
    create_engine, MetaData, engine_from_config, Table, Column,
    Integer, String, ForeignKey, DateTime
)
from sqlalchemy.orm import (
    sessionmaker, scoped_session, mapper, relationship,
)
from sqlalchemy_utils import create_database, drop_database

from src.models import UnitOfWork
from src.models.session import (
    Session, SessionRequest, SessionRepository,
    SessionDoesNotExist, SessionRequestRepository,
)


class SQLAlchemy:

    def __init__(self, engine):
        self.engine = engine
        self._session_maker = scoped_session(sessionmaker(bind=self.engine))

    @classmethod
    def from_config(cls, config, prefix):
        engine = engine_from_config(config, prefix=prefix)
        return cls(engine=engine)

    @classmethod
    def from_uri(cls, uri):
        engine = create_engine(uri)
        return cls(engine=engine)

    @property
    def unit_of_work(self):
        return SQLAlchemyUnitOfWork(self._session_maker)

    def drop_schema(self):
        drop_database(self.engine.url)

    def create_schema(self):
        if not hasattr(self, 'metadata'):
            raise ValueError('You must call `configure`'
                             'before creating schema.')
        else:
            create_database(self.engine.url)
            self.metadata.create_all()

    def configure(self):
        self.metadata = MetaData(self.engine)

        sessions = Table(
            'sessions', self.metadata,
            Column('id', Integer, primary_key=True),
            Column('token', String(50), unique=True),
            Column('browser', String(50)),
        )

        session_requests = Table(
            'session_requests', self.metadata,
            Column('id', Integer, primary_key=True),
            Column('session_id', Integer, ForeignKey('sessions.id')),
            Column('requested_at', DateTime),
            Column('requested_url', String(150))
        )

        mapper(
            Session,
            sessions,
            properties={
                'id': sessions.c.id,
                'token': sessions.c.token,
                'browser': sessions.c.browser,
                'requests': relationship(
                    SessionRequest, backref='session',
                    order_by=session_requests.c.requested_at),
            }
        )

        mapper(
            SessionRequest,
            session_requests,
            properties={
                'id': session_requests.c.id,
                'requested_at': session_requests.c.requested_at,
                'requested_url': session_requests.c.requested_url,
            }
        )


class SQLAlchemyUnitOfWork(UnitOfWork):

    def __init__(self, session_factory):
        self.session_factory = session_factory

    def __enter__(self):
        self.session = self.session_factory()
        return self

    def __exit__(self, type, value, traceback):
        pass

    def commit(self):
        self.session.flush()
        self.session.commit()

    def rollback(self):
        self.session.rollback()

    def remove(self):
        self.session_factory.remove()

    @property
    def session_repository(self):
        return SQLSessionRepository(self.session)

    @property
    def sessionrequest_repository(self):
        return SQLSessionRequestRepository(self.session)


class SQLSessionRepository(SessionRepository):

    def __init__(self, dbsession):
        self.dbsession = dbsession

    def add(self, session):
        self.dbsession.add(session)

    def get(self, session_id):
        session = self.dbsession.query(Session)\
            .filter_by(id=session_id)\
            .first()
        if session is None:
            raise SessionDoesNotExist()
        return session

    def all(self):
        websessions = self.dbsession.query(Session)
        return websessions

    def get_by_browser(self, browser):
        session = self.dbsession.query(Session)\
            .filter_by(browser=browser)\
            .first()
        if session is None:
            raise SessionDoesNotExist()
        return session


class SQLSessionRequestRepository(SessionRequestRepository):

    def __init__(self, dbsession):
        self.dbsession = dbsession

    def add(self, sessionrequest):
        self.dbsession.add(sessionrequest)

    def get_by_session_id(self, session_id):
        sessionrequests = self.dbsession.query(SessionRequest)\
            .filter_by(session_id=session_id)
        return sessionrequests
