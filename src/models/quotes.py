from typing import NamedTuple


class Quote(NamedTuple):
    quote: str


class QuoteNotFound(Exception):
    pass
