import abc


class SessionDoesNotExist(Exception):
    pass


class Session:

    def __init__(self, token, browser):
        self.token = token
        self.browser = browser
        self.requests = []

    def __str__(self):
        return f'<Session(token={self.token}, agent={self.browser})>'


class SessionRepository(abc.ABC):

    @abc.abstractmethod
    def add(self, session):
        pass

    @abc.abstractmethod
    def get(self, session_id):
        pass

    @abc.abstractmethod
    def all(self):
        pass

    @abc.abstractmethod
    def get_by_browser(self, browser):
        pass


class SessionRequest:
    
    def __init__(self, requested_at, requested_url):
        self.requested_at = requested_at
        self.requested_url = requested_url

    def __str__(self):
        return f'<SessionRequest(url={self.requested_url})>'


class SessionRequestRepository(abc.ABC):

    @abc.abstractmethod
    def add(self, sessionrequest):
        pass

    @abc.abstractmethod
    def get_by_session_id(self, session_id):
        pass
