from pyramid.config import Configurator


def main(global_config, **settings):
    config = Configurator(settings=settings)
    config.include('pyramid_services')
    config.include('pyramid_jinja2')
    config.include('pyramid_useragent')
    config.add_jinja2_renderer('.html')
    config.include('.adapters')
    config.include('.services')
    config.include('.views')
    config.scan()
    return config.make_wsgi_app()
