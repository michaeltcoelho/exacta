import pytest
import pyramid.testing
from webtest import TestApp


@pytest.yield_fixture
def config():
    config = pyramid.testing.setUp()
    config.include('pyramid_services')
    config.include('src.adapters')
    config.include('src.models')
    config.include('src.services')
    config.include('src.views')
    yield config
    pyramid.testing.tearDown()


@pytest.fixture
def dummy_request():
    return pyramid.testing.DummyRequest()


@pytest.fixture
def dummy_context():
    return pyramid.testing.DummyResource()


@pytest.fixture
def client(config):
    from src import main
    app = main(settings)
    return TestApp(app)
