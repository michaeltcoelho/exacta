import pytest
import responses

from src.models.quotes import QuoteNotFound
from src.models.session import Session, SessionRequest, SessionDoesNotExist
from src.services import (
    quotes as quotes_services, session as session_services,
)

from .dummies import DummyUnitOfWork


def test_quotes_client_make_uri():
    client = quotes_services.QuotesClient()
    uri = client.make_uri('x', 1, 'z')
    assert uri == f'{client.BASE_URI}/x/1/z'


@responses.activate
def test_quotes_client_get():
    payload = {'foo': 'bar'}

    client = quotes_services.QuotesClient()
    uri = client.make_uri('x', 1, 'z')

    responses.add(responses.GET, uri, json=payload, status=200)

    response = client.get('x', 1, 'z')
    assert response == payload


@responses.activate
def test_quotes_api_get_quotes():
    payload = {'quotes': ['foo']}

    client = quotes_services.QuotesClient()
    quotes_uri = client.make_uri('quotes')

    responses.add(responses.GET, quotes_uri, json=payload, status=200)

    quotes_api = quotes_services.QuotesAPI()
    response = quotes_api.get_quotes()
    assert len(response) == 1
    assert response[0].quote == 'foo'


@responses.activate
def test_quotes_api_get_quote():
    payload = {'quote': 'bar'}

    client = quotes_services.QuotesClient()
    quotes_uri = client.make_uri('quotes', 1)

    responses.add(responses.GET, quotes_uri, json=payload, status=200)

    quotes_api = quotes_services.QuotesAPI()
    response = quotes_api.get_quote(1)
    assert response.quote == payload['quote']


@responses.activate
def test_quotes_api_get_quote_not_found():
    payload = {'quote': 'bar'}

    client = quotes_services.QuotesClient()
    quotes_uri = client.make_uri('quotes', 100)

    responses.add(responses.GET, quotes_uri, json=payload, status=404)

    quotes_api = quotes_services.QuotesAPI()

    with pytest.raises(QuoteNotFound):
        quotes_api.get_quote(100)


@responses.activate
def test_api_quotes_service_get_random_quote():
    client = quotes_services.QuotesClient()

    quotes_uri = client.make_uri('quotes')
    rand_quote_uri = client.make_uri('quotes', 'random')

    quotes_payload = {'quotes': ['foo', 'bar']}
    rand_quote_payload = {'quote': 'foo'}

    responses.add(
        responses.GET, quotes_uri,
        json=quotes_payload, status=200,
    )
    responses.add(
        responses.GET, rand_quote_uri,
        json=rand_quote_payload, status=200,
    )

    quotes_svc = quotes_services.APIQuotesService()
    quote = quotes_svc.get_random_quote()
    assert quote.quote in quotes_payload['quotes']


@responses.activate
def test_api_quotes_service_get_quote():
    client = quotes_services.QuotesClient()

    quote_uri = client.make_uri('quotes', 1)

    payload = {'quote': 'bar'}

    responses.add(
        responses.GET, quote_uri,
        json=payload, status=200,
    )

    quotes_svc = quotes_services.APIQuotesService()
    quote = quotes_svc.get_quote(1)
    assert quote.quote == 'bar'


@responses.activate
def test_api_quotes_service_get_quotes():
    client = quotes_services.QuotesClient()

    quote_uri = client.make_uri('quotes')

    payload = {'quotes': ['bar']}

    responses.add(
        responses.GET, quote_uri,
        json=payload, status=200,
    )

    quotes_svc = quotes_services.APIQuotesService()
    quotes = quotes_svc.get_quotes()
    assert len(quotes) == 1
    assert quotes[0].quote == 'bar'


def test_session_service_create_session():
    uow = DummyUnitOfWork()
    session_svc = session_services.SessionService(uow)
    session_svc.create_new_session('chrome')
    session = uow.session_repository.get_by_browser('chrome')
    assert isinstance(session, Session)
    assert session.browser == 'chrome'


def test_session_service_get_by_browser():
    uow = DummyUnitOfWork()
    uow.session_repository.add(Session('x', 'chrome'))

    session_svc = session_services.SessionService(uow)
    session = session_svc.get_session_by_browser('chrome')
    assert isinstance(session, Session)
    assert session.browser == 'chrome'


def test_session_service_get_by_browser_does_not_exist():
    uow = DummyUnitOfWork()
    session_svc = session_services.SessionService(uow)
    with pytest.raises(SessionDoesNotExist):
        session_svc.get_session_by_browser('chrome')


def test_session_request_service_add_new_session_request():
    uow = DummyUnitOfWork()

    uow.session_repository.add(Session('x', 'chrome'))
    session = uow.session_repository.get_by_browser('chrome')
    session.id = 123

    requested_url = 'http://example.org'
    session_request_svc = session_services.SessionRequestService(uow)
    session_request_svc.add_new_sessionrequest(session, requested_url)

    assert len(session.requests) == 1

    session_request = session.requests[0]
    assert session_request.requested_url == requested_url
