from datetime import datetime
from unittest import mock

import pytest
from pyramid.httpexceptions import HTTPNotFound

from src.models.quotes import Quote
from src.models.session import Session, SessionRequest, SessionDoesNotExist
from src.views import quotes as quotes_views, session as session_views

from .dummies import (
    DummyQuotesService, DummySessionService,
    DummySessionRequestService, DummyUnitOfWork,
)


def test_home_version(dummy_request):
    response = quotes_views.home(dummy_request)
    assert response['version'] == '1.0'


def attach_service_to_request(request, service):
    request.find_service = mock.Mock(return_value=service)


def test_quotes(dummy_request):
    attach_service_to_request(dummy_request, DummyQuotesService())
    response = quotes_views.quotes(dummy_request)
    assert 'quotes' in response
    assert len(response['quotes']) == 2
    assert response['quotes'][0].quote == 'old but gold'


def test_quote(dummy_request):
    attach_service_to_request(dummy_request, DummyQuotesService())
    dummy_request.matchdict = {'quote_number': 0}
    response = quotes_views.quote(dummy_request)
    assert 'quote' in response
    assert isinstance(response['quote'], Quote)
    assert response['quote'].quote == 'old but gold'


def test_quote_not_found(dummy_request):
    attach_service_to_request(dummy_request, DummyQuotesService())
    dummy_request.matchdict = {'quote_number': 100}
    with pytest.raises(HTTPNotFound):
        quotes_views.quote(dummy_request)


def test_random_quote(dummy_request):
    attach_service_to_request(dummy_request, DummyQuotesService())
    response = quotes_views.random_quote(dummy_request)
    assert 'quote' in response
    assert isinstance(response['quote'], Quote)


@mock.patch('src.views.session.get_browser_family_from_request')
def test_session_new_request_handler_session(
        mocked_get_browser_family_from_request):
    mocked_get_browser_family_from_request.return_value = 'chrome'

    dummy_uow = DummyUnitOfWork()
    dummy_session_svc = DummySessionService(dummy_uow)
    dummy_sessionrequest_svc = DummySessionRequestService(dummy_uow)

    def side_effect_services(name):
        if name == 'session':
            return dummy_session_svc
        elif name == 'sessionrequest':
            return dummy_sessionrequest_svc

    dummy_event = mock.Mock()
    dummy_event.request.find_service.side_effect = side_effect_services
    dummy_event.request.url = 'http://example.org'

    session_views.new_request_handler(dummy_event)

    websession = dummy_uow.session_repository.get_by_browser('chrome')

    assert len(websession.requests) == 1
    assert websession.requests[0].requested_url == dummy_event.request.url

    session_views.new_request_handler(dummy_event)

    websession = dummy_uow.session_repository.get_by_browser('chrome')
    assert len(websession.requests) == 2


def test_get_sessions():
    session = Session('x', 'chrome')
    session.id = 123

    dummy_request = mock.Mock()
    dummy_request.find_service().get_sessions.return_value = [session,]

    response = session_views.get_sessions(dummy_request)

    assert 'sessions' in response
    assert response['sessions'][0]['id'] == session.id
    assert response['sessions'][0]['token'] == session.token
    assert response['sessions'][0]['browser'] == session.browser


def get_sessionrequests_by_session_id():
    sessionrequest = SessionRequest(datetime.utcnow(), 'http://example.org')
    sessionrequest.id = 123

    dummy_request = mock.Mock()
    dummy_request.find_service()\
        .get_sessionrequests_by_session_id.return_value = [sessionrequest,]

    response = session_views.get_sessionrequests_by_session_id(dummy_request)

    assert 'requests' in response
    assert response['requests'][0]['id'] == sessionrequest.id
    assert response['requests'][0]['requested_at'] ==\
        sessionrequest.requested_at.strftime('%Y-%m-%d')
    assert response['requests'][0]['requested_url'] ==\
        sessionrequest.requested_url


def get_sessionrequests_by_session_id_not_found():
    dummy_request = mock.Mock()
    dummy_request.find_service()\
        .get_sessionrequests_by_session_id.side_effect = SessionDoesNotExist()
    with pytest.raises(HTTPNotFound):
        session_views.get_sessionrequests_by_session_id(dummy_request)
