import random
from datetime import datetime
from operator import attrgetter

from src.models import UnitOfWork
from src.models.quotes import Quote, QuoteNotFound
from src.models.session import (
    SessionRepository, SessionDoesNotExist, SessionRequestRepository,
    Session, SessionRequest,
)


class DummyQuotesService:

    def __init__(self):
        self.QUOTES = [
            Quote(quote='old but gold'),
            Quote(quote='hello world.'),
        ]

    def get_quotes(self):
        return self.QUOTES

    def get_quote(self, quote_number):
        try:
            return self.QUOTES[quote_number]
        except IndexError as ex:
            raise QuoteNotFound()

    def get_random_quote(self):
        index = random.randint(0, len(self.QUOTES) - 1)
        return self.QUOTES[index]


class DummySessionService:

    def __init__(self, uow):
        self.unit_of_work = uow

    def get_session_by_browser(self, browser):
        return self.unit_of_work.session_repository.get_by_browser(browser)

    def create_new_session(self, browser):
        session = Session('x', browser)
        self.unit_of_work.session_repository.add(session)


class DummySessionRequestService:

    def __init__(self, uow):
        self.unit_of_work = uow

    def add_new_sessionrequest(self, session, requested_url):
        sessionrequest = SessionRequest(
            requested_at=datetime.utcnow(),
            requested_url=requested_url)
        session.requests.append(sessionrequest)
        self.unit_of_work.session_repository.add(session)


class DummyUnitOfWork(UnitOfWork):

    def __init__(self):
        self._session_repo = DummySessionRepository()
        self._session_request_repo = DummySessionRequestRepository()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass

    def commit(self):
        self.was_commited = True

    def rollback(self):
        self.was_rolled_back = True

    @property
    def session_repository(self):
        return self._session_repo

    @property
    def sessionrequest_repository(self):
        return self._session_request_repo


class BaseDummyRepository:
    does_not_exist_exception = Exception

    def __init__(self):
        self._data = []

    def add(self, item):
        self._data.append(item)

    def _get_by(self, field, value):
        item_matched = None
        for item in self._data:
            if attrgetter(field)(item) == value:
                item_matched = item
        if item_matched is None:
            raise self.does_not_exist_exception()
        return item_matched

    def get(self, item_id):
        return self._get_by('id', item_id)

    def all(self):
        return self._data


class DummySessionRepository(BaseDummyRepository, SessionRepository):
    does_not_exist_exception = SessionDoesNotExist

    def get_by_browser(self, browser):
        return self._get_by('browser', browser)


class DummySessionRequestRepository(
    BaseDummyRepository, SessionRequestRepository):

    def get_by_session_id(self, session_id):
        return self._get_by('session_id', session_id)
