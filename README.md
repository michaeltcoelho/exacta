# Teste Exacta

## OBS

Não consegui me dedicar muito tempo para o teste, contudo, tentei implementá-lo
utilizando boas práticas. Lembrando que nunca trabalhei com Pyramid e nem sequer
tive contato, essa é minha primeira vez.

Pontos que poderia trabalhar mais:

- Tratamento de erros
- Testes funcionais e de integração.
- Nomes mais semanticos com o dominio (Clean Code).
- Trabalhar com UUID nas urls e não expor PKs.
- Migrations

### Requisitos

* Uma virtualenv com Python 3.6+.

### Instalando

Clone o repositório e instale o pacote:

`$ git clone https://michaeltcoelho@bitbucket.org/michaeltcoelho/exacta.git`

Navegue para o diretório `/exacta`:

`cd exacta`

Execute o seguinte commando:

`make install`

### Inicializando Banco de Dados

Para inicializar o banco de dados:

`make initdb`

### Excecutando testes

Para executar os testes:

`make run_test`

### Rodando a aplicação web:

Para rodar a aplicação web:

`make run`


## API

A Aplicação está rodando em `localhost:8000`

Endpoints para as telas de `Quotes`.

- `/quotes`
- `/quotes/<quote_number>`
- `/quotes/random`


Endpoints para a API das sessões registradas:

- `/sessions`
- `/sessions/<session_id>`
- `/sessions/<session_id>/requests`
