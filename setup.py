from setuptools import setup, find_packages

requires = [
    'pyramid >= 1.9a',
    'pyramid_jinja2==2.7',
    'pyramid-services==1.1',
    'pyramid-sqlalchemy==1.6',
    'SQLAlchemy==1.2.7',
    'waitress==1.1.0',
    'requests == 2.18.4',
    'SQLAlchemy-Utils==0.33.2',
    'pyramid-useragent==0.3.1',
]

tests_require = [
    'WebTest >= 1.3.1',  # py3 compat
    'pytest==3.5.0',
    'responses==0.9.0',
    'pytest-mock==1.9.0',
]

setup(
    name='exacta',
    version='0.0',
    description='Job test',
    classifiers=[
        'Programming Language :: Python 3.6.4',
        'Framework :: Pyramid',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ],
    author='Michael',
    author_email='michael.tcoelho@gmail.com',
    url='https://bitbucket.org/michaeltcoelho/exacta',
    keywords='web pyramid pylons',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    extras_require={
        'testing': tests_require,
    },
    install_requires=requires,
    entry_points={
        'paste.app_factory': [
            'main = src:main',
        ],
        'console_scripts': [
            'init_db = src.manage:init_db',
        ],
    },
)
